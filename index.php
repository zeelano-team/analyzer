<?php

define("BASEPATH", 1);

$mscus["analyzer"]["version"]   = 0.1;
$mscus["analyzer"]["debug"]     = true;

//include trailing slash
$mscus["base_url"]["https://mscus.mj-mueller.de/analyzer/"];

include("inc/util/base_url.php");
include("inc/util/pretty_var_dump.php");

//mysql
$mscus["mysql"]["host"]             = "178.254.6.70";
$mscus["mysql"]["username"]         = "mscus_test_user";
$mscus["mysql"]["password"]         = "DHw3bW47K3ChVLW9";
$mscus["mysql"]["database_name"]    = "mscus_testing";
$mscus["mysql"]["port"]             = 3306;

//test db connection
if (!function_exists("mysqli_connect")) {
    die ("MySQL was not found on this server. You might have to enable it in your php.ini");
}
$mscus["db"] = new mysqli(
    $mscus["mysql"]["host"],
    $mscus["mysql"]["username"],
    $mscus["mysql"]["password"],
    $mscus["mysql"]["database_name"],
    $mscus["mysql"]["port"]
);

//checking connection
if ($mscus["db"]->connect_errno) {
    die ("Failed to connect to MySQL: " . $mscus["db"]->connect_errno);
}

//checking if clicks table exists
$query = "SHOW TABLES LIKE 'clicks'";
$result = $mscus["db"]->query($query);

if (!$result || $result->num_rows==0) {
    //no table found, create it
    die ("No data found. Collect some!");
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <title>MSCUS::analyzer</title>

    <link href="<?=base_url("public/metroui/css/metro.css")?>" rel="stylesheet">
</head>
<body>

<div class="app-bar">
    <div class="container">
        <a class="app-bar-element" href="#">MSCUS</a>
    </div>
</div>

<div class="container">
    <h1>Your sites</h1>

    <table class="table striped hovered dataTable" data-role="datatable">
        <thead>
            <tr>
                <th class="sortable-column">site identifier</th>
                <th class="sortable-column">clicks (total)</th>
                <th class="sortable-column">unique users</th>
            </tr>
        </thead>
        <tbody>
    <?php

    //$query = "SELECT DISTINCT `identifier` FROM `clicks` ORDER BY `identifier` ASC";
    //$query = "SELECT `identifier`, COUNT(*) AS `site_clicks` FROM `clicks` GROUP BY `identifier` ORDER BY
    // `site_clicks` DESC";
    $query = "SELECT `identifier`, COUNT(*) AS `site_clicks`, COUNT(DISTINCT(`tracking_cookie`)) AS `unique_users` FROM `clicks` GROUP BY `identifier` ORDER BY `site_clicks` DESC";

    $result = $mscus["db"]->query($query);
    if ($mscus["debug"] && !$result) {
        pretty_var_dump($mscus["db"]->error);
        exit;
    }

    $result->data_seek(0);
    while ($row = $result->fetch_assoc()) {

        $mscus["data"]["site_overview"][] = $row;

        echo <<<HTML

<tr>
    <td>{$row["identifier"]}</td>
    <td>{$row["site_clicks"]}</td>
    <td>{$row["unique_users"]}</td>
</tr>

HTML;


    }

    ?>
        </tbody>
    </table>
    <br><br><br>

    <?php
    $query = "SELECT `request_timestamp` FROM `clicks` ORDER BY `request_timestamp` ASC LIMIT 1";

    $result = $mscus["db"]->query($query);
    if ($mscus["debug"] && !$result) {
        pretty_var_dump($mscus["db"]->error);
        exit;
    }

    $mscus["data"]["data_since"] = $result->fetch_assoc()["request_timestamp"];

    $date = new DateTime($mscus["data"]["data_since"]);
    ?>

    <span class="padding10 bg-grayLight fg-white place-right">Showing data since
        <span class="bg-steel text-shadow fg-white padding10"><?=$date->format("D. d. M y")?></span></span>
    <span class="clear-float"></span>

</div>


<script src="<?=base_url("public/jquery/jquery.min.js")?>"></script>
<script src="<?=base_url("public/dataTables/jquery.dataTables.min.js")?>"></script>
<script src="<?=base_url("public/metroui/js/metro.min.js")?>"></script>

</body>
</html>