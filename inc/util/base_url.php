<?php
if (!defined("BASEPATH")) die ("No direct script access allowed");

function base_url($par1 = "") {
    global $mscus;
    return $mscus["base_url"] . $par1;
}